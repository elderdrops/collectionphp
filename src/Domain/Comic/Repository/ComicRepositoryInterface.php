<?php
/**
 * Created by PhpStorm.
 * User: ElderDrop
 * Date: 28.10.18
 * Time: 11:52
 */

namespace App\Domain\Comic\Repository;


use App\Domain\Comic\Comic;
use App\Domain\Comic\ValueObject\Title;

interface ComicRepositoryInterface
{
    /**
     * @param Title $title
     * @return Comic|null
     */
    public function findOneByTitle(Title $title): ?Comic;

    /**
     * @param Comic $comic
     */
    public function saveComic(Comic $comic): void;
}