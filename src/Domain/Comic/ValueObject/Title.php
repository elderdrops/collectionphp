<?php
/**
 * Created by PhpStorm.
 * User: ElderDrop
 * Date: 28.10.18
 * Time: 11:58
 */

namespace App\Domain\Comic\ValueObject;

use Doctrine\ORM\Mapping as ORM;

/** @ORM\Embeddable */
final class Title
{
    /**
     * @ORM\Column(type = "string", length=180, unique=false)
     * @var string
     */
    public $title;

    /**
     * @param string $title
     * @return Title
     */
    public static function fromString(string $title): self
    {
        $model = new self();
        $model->title = $title;

        return $model;
    }

    /**
     * @return string
     */
    public function toString(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->toString();
    }


}