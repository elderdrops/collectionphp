<?php
/**
 * Created by PhpStorm.
 * User: ElderDrop
 * Date: 18.11.18
 * Time: 10:05
 */

namespace App\Domain\Comic\ValueObject;

use Doctrine\ORM\Mapping as ORM;

/** @ORM\Embeddable */
final class Description
{
    /**
     * @ORM\Column(type = "string", length=250, unique=false)
     * @var string
     */
    public $description;

    /**
     * @param string $description
     * @return Description
     */
    public static function fromString(string $description): self
    {
        $model = new self;
        $model->description = $description;

        return $model;
    }
}