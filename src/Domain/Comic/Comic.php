<?php

namespace App\Domain\Comic;

use App\Domain\Author\Author;
use App\Domain\Comic\ValueObject\Description;
use App\Domain\Comic\ValueObject\Title;
use App\Domain\User\User;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Infrastructure\Comic\Repository\ComicRepository")
 */
class Comic
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /** @ORM\Embedded(class = "App\Domain\Comic\ValueObject\Title") */
    private $title;

    /** @ORM\Embedded(class = "App\Domain\Comic\ValueObject\Description") */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity="App\Domain\Author\Author", mappedBy="comics")
     */
    private $authors;

    /**
     * @ORM\ManyToMany(targetEntity="App\Domain\User\User", mappedBy="comics")
     */
    private $owners;

    public function __construct()
    {
        $this->title = Title::fromString('');
        $this->authors = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Title
     */
    public function getTitle(): Title
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    public function getAuthors()
    {
        return $this->authors;
    }
    /**
     * @param Author $author
     */
    public function addAuthor(Author $author): void
    {
        $author->addComic($this);
        $this->authors[] = $author;
    }

    /**
     * @param User $user
     */
    public function addUser(User $user)
    {
        $this->owners[] = $user;
    }

    /**
     * @return Description
     */
    public function getDescription(): Description
    {
        return $this->description;
    }

    /**
     * @param Description $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }
}