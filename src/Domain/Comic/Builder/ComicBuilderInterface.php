<?php
/**
 * Created by PhpStorm.
 * User: ElderDrop
 * Date: 28.10.18
 * Time: 13:29
 */

namespace App\Domain\Comic\Builder;


use App\Domain\Comic\Comic;
use App\Domain\Comic\Exception\NotUniqueTitleException;
use Symfony\Component\HttpFoundation\Request;

interface ComicBuilderInterface
{
    /**
     * @param Request $request
     * @return Comic
     */
    public function build(Request $request): Comic;
}