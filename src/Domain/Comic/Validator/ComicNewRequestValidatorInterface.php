<?php
/**
 * Created by PhpStorm.
 * User: ElderDrop
 * Date: 28.10.18
 * Time: 13:36
 */

namespace App\Domain\Comic\Validator;


use App\Domain\Comic\Exception\NotUniqueTitleException;
use Symfony\Component\HttpFoundation\Request;

interface ComicNewRequestValidatorInterface
{
    /**
     * @param Request $request
     * @throws NotUniqueTitleException
     */
    public function isValid(Request $request): void;
}