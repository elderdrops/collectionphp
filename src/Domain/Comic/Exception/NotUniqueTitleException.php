<?php
/**
 * Created by PhpStorm.
 * User: ElderDrop
 * Date: 28.10.18
 * Time: 13:46
 */

namespace App\Domain\Comic\Exception;


use App\Domain\Comic\Comic;

class NotUniqueTitleException extends \Exception
{
    /** @var Comic */
    private $comic;

    public function setComic(Comic $comic): void
    {
        $this->comic = $comic;
    }
}