<?php
/**
 * Created by PhpStorm.
 * User: ElderDrop
 * Date: 13.11.18
 * Time: 21:37
 */

namespace App\Domain\Comic\Service;


use App\Domain\Comic\Comic;
use App\Domain\User\User;

interface AssignServiceInterface
{
    public function assignOwnerToComic(Comic $comic, User $user): void;
}