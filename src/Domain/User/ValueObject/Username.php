<?php
/**
 * Created by PhpStorm.
 * User: ElderDrop
 * Date: 20.10.18
 * Time: 14:22
 */

namespace App\Domain\User\ValueObject;

use Doctrine\ORM\Mapping as ORM;


/** @ORM\Embeddable */
final class Username
{
    /** @ORM\Column(type = "string", length=180, unique=true) */
    public $username;

    /**
     * @param string $username
     * @return Username
     */
    public static function fromString(string $username): self
    {
        $model = new self;
        $model->username = $username;

        return $model;
    }

    /**
     * @return string
     */
    public function toString(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->toString();
    }


}