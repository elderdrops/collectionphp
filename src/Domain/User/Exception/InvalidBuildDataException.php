<?php
/**
 * Created by PhpStorm.
 * User: ElderDrop
 * Date: 27.10.18
 * Time: 09:04
 */

namespace App\Domain\User\Exception;


class InvalidBuildDataException extends \Exception
{
    private $data;

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data): void
    {
        $this->data = $data;
    }
}