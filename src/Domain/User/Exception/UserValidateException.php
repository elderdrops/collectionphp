<?php
/**
 * Created by PhpStorm.
 * User: ElderDrop
 * Date: 28.10.18
 * Time: 09:03
 */

namespace App\Domain\User\Exception;


class UserValidateException extends \Exception
{
    /**
     * @var array
     */
    private $errors;

    /**
     * UserValidateException constructor.
     * @param array $errors
     * @param string $msg
     */
    public function __construct(array $errors, string $msg = '')
    {
        $this->errors = $errors;
        parent::__construct($msg);
    }

    /** @return array */
    public function getErrors(): array
    {
        return $this->errors;
    }
}