<?php

namespace App\Domain\User\Specification;


use App\Domain\User\Exception\SpecificationException;
use App\Domain\User\Repository\UserRepositoryInterface;
use App\Infrastructure\User\Facade\Register\UserRegisterData;

final class UsernameUniquenessSpecification implements SpecificationInterface
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * UsernameUniquenessSpecification constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /** @inheritdoc */
    public function isSatisfiedBy(UserRegisterData $registerData): void
    {
        if($this->userRepository->findOneByUsername($registerData->getUsername()))
        {
            throw new SpecificationException('User with this username already exists');
        }
    }
}