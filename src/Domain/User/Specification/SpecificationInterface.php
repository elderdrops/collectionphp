<?php
/**
 * Created by PhpStorm.
 * User: ElderDrop
 * Date: 17.11.18
 * Time: 14:21
 */

namespace App\Domain\User\Specification;


use App\Domain\User\Exception\SpecificationException;
use App\Infrastructure\User\Facade\Register\UserRegisterData;

interface SpecificationInterface
{
    /**
     * @param UserRegisterData $registerData
     * @throws SpecificationException
     */
    public function isSatisfiedBy(UserRegisterData $registerData): void;
}