<?php
/**
 * Created by PhpStorm.
 * User: ElderDrop
 * Date: 17.11.18
 * Time: 14:23
 */

namespace App\Domain\User\Specification;


use App\Domain\User\Exception\SpecificationException;
use App\Infrastructure\User\Facade\Register\UserRegisterData;

final class UsernameLengthSpecification implements SpecificationInterface
{
    /** @var int  */
    private const MIN_LENGTH = 4;

    /** @inheritdoc */
    public function isSatisfiedBy(UserRegisterData $registerData): void
    {
        $username = $registerData->getUsername();

        if(strlen($username->toString()) < self::MIN_LENGTH)
        {
            throw new SpecificationException("User name should me longer then 4 chars");
        }
    }
}