<?php
/**
 * Created by PhpStorm.
 * User: ElderDrop
 * Date: 21.10.18
 * Time: 14:26
 */

namespace App\Domain\User\Validator;


use App\Domain\User\Facade\Register\UserDataRegisterInterface;
use App\Infrastructure\User\Facade\Register\UserRegisterData;

interface UserDataValidatorInterface
{
    /**
     * @param UserRegisterData $userData
     * @return bool
     */
    public function validate(UserRegisterData $userData): bool;

    /** @return array|null */
    public function getErrors(): ?array ;
}