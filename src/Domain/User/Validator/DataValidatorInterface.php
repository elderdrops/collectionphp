<?php

namespace App\Domain\User\Validator;

use App\Infrastructure\User\Facade\Register\UserRegisterData;

interface DataValidatorInterface
{
    /**
     * @param UserRegisterData $data
     * @return bool
     */
    public function isValid(UserRegisterData $data): bool;

    /** @return string */
    public function getFieldName(): string;

    /** @return array */
    public function getErrors(): array;
}