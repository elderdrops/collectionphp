<?php

namespace App\Domain\User\Validator;

use App\Domain\User\Exception\SpecificationException;
use App\Domain\User\Specification\UsernameLengthSpecification;
use App\Domain\User\Specification\UsernameUniquenessSpecification;
use App\Infrastructure\User\Facade\Register\UserRegisterData;

class UsernameValidatorInterface implements DataValidatorInterface
{
    /**
     * @var UsernameLengthSpecification
     */
    private $usernameSpecification;
    /** @var SpecificationException[] */
    private $errors;
    /**
     * @var UsernameUniquenessSpecification
     */
    private $usernameUniquenessSpecification;

    /**
     * UsernameValidator constructor.
     * @param UsernameLengthSpecification $usernameSpecification
     * @param UsernameUniquenessSpecification $usernameUniquenessSpecification
     */
    public function __construct(UsernameLengthSpecification $usernameSpecification, UsernameUniquenessSpecification $usernameUniquenessSpecification)
    {
        $this->usernameSpecification = $usernameSpecification;
        $this->usernameUniquenessSpecification = $usernameUniquenessSpecification;
    }

    /**
     * @param UserRegisterData $data
     * @return bool
     */
    public function isValid(UserRegisterData $data): bool
    {
        $isValid = true;
        try
        {
            $this->usernameSpecification->isSatisfiedBy($data);
            $this->usernameUniquenessSpecification->isSatisfiedBy($data);
        }
        catch (SpecificationException $e)
        {
            $this->addError($e);
            $isValid = false;
        }


        return $isValid;
    }

    /**
     * @param SpecificationException $e
     */
    private function addError(SpecificationException $e)
    {
        $this->errors[] = $e;
    }

    /** @return string */
    public function getFieldName(): string
    {
        return 'username';
    }

    /** @return SpecificationException[] */
    public function getErrors(): array
    {
        return $this->errors;
    }
}