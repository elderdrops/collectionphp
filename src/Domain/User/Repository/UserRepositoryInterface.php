<?php

namespace App\Domain\User\Repository;


use App\Domain\User\User;
use App\Domain\User\ValueObject\Username;

interface UserRepositoryInterface
{
    /**
     * @param Username $username
     * @return User
     */
    public function findOneByUsername(Username $username): ?User;

    /**
     * @param User $user
     */
    public function saveUser(User $user): void;
}