<?php

namespace App\Domain\User;

use App\Domain\Comic\Comic;
use App\Domain\User\ValueObject\Username as Username;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

// TODO: Pass it to infra :D
/**
 * @ORM\Entity(repositoryClass="App\Infrastructure\User\Repository\UserRepository")
 */
class User implements UserInterface
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /** @ORM\Embedded(class = "App\Domain\User\ValueObject\Username") */
    public $username;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\ManyToMany(targetEntity="App\Domain\Comic\Comic", inversedBy="owners")
     * @ORM\JoinTable(name="comics_users")
     */
    private $comics;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->username = new Username();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    //TODO: Ogarnij to na value objetct cos z logowaniem sie dzieje
    public function getUsername(): string
    {
        return $this->username->toString();
    }

    public function setUsername(Username $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function addComic(Comic $comic): void
    {
        $comic->addUser($this);
        $this->comics[] = $comic;
    }
    /**
     * @see UserInterface
     */
    public function getSalt(){}

    /**
     * @see UserInterface
     */
    public function eraseCredentials(){}
}
