<?php


namespace App\Domain\User\Builder;


use App\Domain\User\User;

interface UserBuilderInterface
{
    public function build($data): User;
}