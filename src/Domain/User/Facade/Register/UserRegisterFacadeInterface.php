<?php
/**
 * Created by PhpStorm.
 * User: ElderDrop
 * Date: 21.10.18
 * Time: 14:14
 */

namespace App\Domain\User\Facade\Register;

use App\Domain\User\User;
use App\Domain\User\Validator\UserDataValidatorInterface;
use App\Infrastructure\User\Facade\Register\UserRegisterData;

interface UserRegisterFacadeInterface
{
    /**
     * @param UserRegisterData $data
     * @return User
     */
    public function register(UserRegisterData $data): User;
}