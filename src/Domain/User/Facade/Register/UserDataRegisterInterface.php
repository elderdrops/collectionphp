<?php

namespace App\Domain\User\Facade\Register;


use App\Domain\User\Validator\UserDataValidatorInterface;
use App\Domain\User\ValueObject\Username;

interface UserDataRegisterInterface
{
    /**
     * @param UserDataValidatorInterface $validator
     * @return bool
     */
//    public function isValid(UserDataValidatorInterface $validator): bool;

    /**
     * @return Username
     */
    public function getUsername(): Username;

    /**
     * @return string
     */
    public function getPassword(): string;
}