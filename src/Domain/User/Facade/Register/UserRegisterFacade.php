<?php

namespace App\Domain\User\Facade\Register;


use App\Domain\User\Builder\UserBuilderInterface;
use App\Domain\User\Exception\InvalidBuildDataException;
use App\Domain\User\Exception\UserValidateException;
use App\Domain\User\Facade\Register\UserDataRegisterInterface;
use App\Domain\User\Facade\Register\UserRegisterFacadeInterface;
use App\Domain\User\User;
use App\Domain\User\Validator\UserDataValidatorInterface;
use App\Infrastructure\User\Facade\Register\UserRegisterData;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class UserRegisterFacade implements UserRegisterFacadeInterface
{
    /**
     * @var UserDataValidatorInterface
     */
    private $validator;
    /**
     * @var UserBuilderInterface
     */
    private $builder;
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * UserRegisterFacade constructor.
     * @param UserDataValidatorInterface $validator
     * @param UserBuilderInterface $builder
     * @param EntityManagerInterface $em
     */
    public function __construct(UserDataValidatorInterface $validator,
                                UserBuilderInterface $builder,
                                EntityManagerInterface $em)
    {
        $this->validator = $validator;
        $this->builder = $builder;
        $this->em = $em;
    }

    /**
     * @param UserDataRegisterInterface $data
     * @return User
     * @throws UserValidateException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function register(UserRegisterData $data): User
    {

        if($this->validator->validate($data) === false)
        {
            throw new UserValidateException($this->validator->getErrors(), 'Can not create user');
        }

        $user = $this->builder->build($data);
        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }
}