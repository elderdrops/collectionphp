<?php
/**
 * Created by PhpStorm.
 * User: ElderDrop
 * Date: 28.10.18
 * Time: 09:29
 */

namespace App\Domain\Author;

use App\Domain\Author\ValueObject\Name;
use App\Domain\Comic\Comic;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Infrastructure\Author\Repository\AuthorRepository")
 */
class Author
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /** @ORM\Embedded(class = "App\Domain\Author\ValueObject\Name") */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Domain\Comic\Comic", inversedBy="authors")
     * @ORM\JoinTable(name="comics_authors")
     */
    private $comics;

    /**
     * Author constructor.
     */
    public function __construct()
    {
        $this->name = new Name('','');
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    public function addComic(Comic $comic)
    {
        $this->comics[] = $comic;
    }
}