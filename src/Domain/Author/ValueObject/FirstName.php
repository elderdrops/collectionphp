<?php
/**
 * Created by PhpStorm.
 * User: ElderDrop
 * Date: 28.10.18
 * Time: 09:36
 */

namespace App\Domain\Author\ValueObject;


use Symfony\Component\Security\Http\EntryPoint\RetryAuthenticationEntryPoint;

final class FirstName
{
    /**
     * @var string
     */
    private $firstName;

    /**
     * @param string $firstName
     * @return FirstName
     */
    public static function fromString(string $firstName): self
    {
        $model = new self();
        $model->firstName = $firstName;

        return $model;
    }
}