<?php
/**
 * Created by PhpStorm.
 * User: ElderDrop
 * Date: 28.10.18
 * Time: 09:39
 */

namespace App\Domain\Author\ValueObject;

use Doctrine\ORM\Mapping as ORM;


/** @ORM\Embeddable */
class Name
{
    /**
     * @ORM\Column(type = "string", length=180, unique=false)
     * @var string
     */
    public $firstName;
    /**
     * @ORM\Column(type = "string", length=180, unique=false)
     * @var string
     */
    public $lastName;

    public function __construct(string $firstName, string $lastName)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }
}