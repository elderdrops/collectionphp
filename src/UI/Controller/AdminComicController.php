<?php
/**
 * Created by PhpStorm.
 * User: ElderDrop
 * Date: 13.11.18
 * Time: 22:28
 */

namespace App\UI\Controller;


use App\Domain\Comic\Builder\ComicBuilderInterface;
use App\Domain\Comic\Exception\NotUniqueTitleException;
use App\Domain\Comic\Repository\ComicRepositoryInterface;
use App\Domain\Comic\Validator\ComicNewRequestValidatorInterface;
use Assert\AssertionFailedException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminComicController
 * @package App\UI\Controller
 *  @Route(
 *     "/admin/comics"
 * )
 */
class AdminComicController extends Controller
{
    /**
     * @Route(
     *     "/new",
     *     name="admin_comic_new",
     *     methods={"GET","POST"}
     * )
     *
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Request $request
     * @param ComicNewRequestValidatorInterface $validator
     * @param ComicRepositoryInterface $repository
     * @param ComicBuilderInterface $builder
     * @return Response
     */
    public function newAction(Request $request,
                              ComicNewRequestValidatorInterface $validator,
                              ComicRepositoryInterface $repository,
                              ComicBuilderInterface $builder): Response
    {

        try
        {
            $validator->isValid($request);
            $comic = $builder->build($request);
            $repository->saveComic($comic);
        }
        catch (NotUniqueTitleException $exception)
        {
            $error = $exception->getMessage();
            return $this->render('comic/new.html.twig',[
                'error' => $error,
            ]);
        }
        catch (AssertionFailedException $exception)
        {
            return $this->render('comic/new.html.twig',[
                //TODO: errory
            ]);
        }
        // TODO: redirect TO SHOW
        return $this->redirectToRoute('comic_show',[
            'id' => $comic->getId(),
        ]);
    }
}