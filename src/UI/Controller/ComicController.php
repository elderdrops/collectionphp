<?php

declare(strict_types=1);

namespace App\UI\Controller;


use App\Domain\Comic\Builder\ComicBuilderInterface;
use App\Domain\Comic\Comic;
use App\Domain\Comic\Exception\NotUniqueTitleException;
use App\Domain\Comic\Repository\ComicRepositoryInterface;
use App\Domain\Comic\Service\AssignServiceInterface;
use App\Domain\Comic\Validator\ComicNewRequestValidatorInterface;
use App\Infrastructure\Comic\Builder\ComicBuilder;
use Assert\AssertionFailedException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;

class ComicController extends Controller
{
    const COMIC_PER_PAGE = 20;

    /**
     * @Route(
     *     "/comics",
     *     name="comic_index",
     *     methods={"GET"}
     * )
     * @param EntityManagerInterface $entityManager
     * @param Request $request
     * @return Response
     */
    public function indexAction(EntityManagerInterface $entityManager, Request $request): Response
    {
        $requestMultiplier = $request->query->getInt('page', 0);
        if ($requestMultiplier > 0)
        {
            $requestMultiplier -= 1;
        }
        $offset = self::COMIC_PER_PAGE * $requestMultiplier;

        $comics = $entityManager->getRepository(Comic::class)->findBy([],[],self::COMIC_PER_PAGE, $offset);
        return $this->render('comic/index.html.twig', [
            'comics' => $comics,
        ]);
    }

    /**
     * @Route(
     *     "/comics/{id}",
     *     name="comic_show",
     *     methods={"GET","POST"}
     * )
     * @param Comic $comic
     * @return Response
     */
    public function showAction(Comic $comic): Response
    {
        return $this->render('comic/show.html.twig',[
            'comic' => $comic,
        ]);
    }

    /**
     * @Route(
     *     "/comics/{id}/assign",
     *     name="comic_assign",
     *     methods={"GET","POST"}
     * )
     *
     * @IsGranted("ROLE_USER")
     *
     * @param Comic $comic
     * @param AssignServiceInterface $service
     * @return Response
     */
    public function assignOwnerAction(Comic $comic, AssignServiceInterface $service): Response
    {
        try
        {
            $service->assignOwnerToComic($comic, $this->getUser());
            return $this->render('comic/show.html.twig',[
                'comic' => $comic,
            ]);
        }
        catch (\Exception $exception)
        {
            return $this->render('comic/show.html.twig',[
                'error' => 'error',
                'comic' => $comic,
            ]);
        }
    }
}