<?php

namespace App\UI\Controller;

use App\Domain\User\Exception\UserValidateException;
use App\Domain\User\Facade\Register\UserRegisterFacadeInterface;
use App\Domain\User\ValueObject\Username;
use App\Infrastructure\User\Facade\Register\UserRegisterData;
use App\Infrastructure\User\Security\LoginFormAuthenticator;
use Assert\Assertion;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
        ]);
    }

    /**
     * @Route(
     *     "/register",
     *     name="register",
     *     methods={"GET", "POST"}
     * )
     * @param Request $request
     * @param UserRegisterFacadeInterface $userRegister
     * @param GuardAuthenticatorHandler $guardHandler
     * @param LoginFormAuthenticator $authenticator
     * @return Response
     * @throws \Assert\AssertionFailedException
     */
    public function register(Request $request, UserRegisterFacadeInterface $userRegister,GuardAuthenticatorHandler $guardHandler,LoginFormAuthenticator $authenticator)
    {
        $username = $request->request->get('username');
        $password = $request->request->get('password');

        try
        {
            Assertion::notNull($username);
            Assertion::notNull($password);

            $user = $userRegister->register(new UserRegisterData(
                Username::fromString($username),
                $password
            ));
        }
        catch (UserValidateException $exception)
        {
            return $this->render('security/register.html.twig', [
                'error' => $exception->getMessage(),
                'userErrors' => $exception->getErrors(),
                'last_username' => $username,
            ]);
        }
        catch (\InvalidArgumentException $exception)
        {
            return $this->render('security/register.html.twig', [
                    'error' => $exception->getMessage(),
                    'last_username' => $username,
                    ]);
        }

       return $guardHandler->authenticateUserAndHandleSuccess(
            $user,
            $request,
            $authenticator,
            'main'
        );
    }
}
