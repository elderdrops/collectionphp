<?php


declare(strict_types=1);

namespace App\UI\Controller;

use App\Domain\Author\Author;
use App\Domain\Author\Repository\AuthorRepositoryInterface;
use App\Infrastructure\Author\Repository\AuthorRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AuthorController extends Controller
{
    /**
     * @Route(
     *     "/authors",
     *     name="authors",
     *     methods={"GET"}
     * )
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function indexAction(EntityManagerInterface $entityManager): Response
    {
        $authors = $entityManager->getRepository(Author::class)->findAll();
        return $this->render('author/index.html.twig', [
                'authors' => $authors,
            ]);
    }
}