<?php
/**
 * Created by PhpStorm.
 * User: ElderDrop
 * Date: 06.10.18
 * Time: 14:15
 */

declare(strict_types=1);

namespace App\UI\Controller;


use Doctrine\DBAL\Connection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;


class IndexController extends Controller
{
    /**
     * @Route(
     *     "/",
     *     name="homepage",
     *     methods={"GET"}
     * )
     */
    public function indexAction(): Response
    {
        return $this->render('default/index.html.twig');
    }
}