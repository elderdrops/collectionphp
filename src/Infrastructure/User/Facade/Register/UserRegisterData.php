<?php
/**
 * Created by PhpStorm.
 * User: ElderDrop
 * Date: 21.10.18
 * Time: 14:19
 */

namespace App\Infrastructure\User\Facade\Register;


use App\Domain\User\Facade\Register\UserDataRegisterInterface;
use App\Domain\User\Validator\UserDataValidatorInterface;
use App\Domain\User\ValueObject\Username;

class UserRegisterData implements UserDataRegisterInterface
{
    /**
     * @var string
     */
    private $password;
    /**
     * @var Username
     */
    private $username;

    /**
     * UserRegisterData constructor.
     * @param Username $username
     * @param string $password
     */
    public function __construct(Username $username, string $password)
    {
        $this->password = $password;
        $this->username = $username;
    }

    /**
     * @return Username
     */
    public function getUsername(): Username
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }
}