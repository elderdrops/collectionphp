<?php
/**
 * Created by PhpStorm.
 * User: ElderDrop
 * Date: 21.10.18
 * Time: 14:27
 */

namespace App\Infrastructure\User\Validator;


use App\Domain\User\Exception\SpecificationException;
use App\Domain\User\Facade\Register\UserDataRegisterInterface;
use App\Domain\User\Validator\UserDataValidatorInterface;
use App\Domain\User\Validator\UsernameValidatorInterface;
use App\Domain\User\ValueObject\Username;
use App\Infrastructure\User\Facade\Register\UserRegisterData;

class UserDataValidator implements UserDataValidatorInterface
{
    /**
     * @var UsernameValidatorInterface
     */
    private $usernameValidator;
    /** @var array  */
    private $errors;

    /**
     * UserDataValidator constructor.
     * @param UsernameValidatorInterface $usernameValidator
     */
    public function __construct(UsernameValidatorInterface $usernameValidator)
    {
        $this->usernameValidator = $usernameValidator;
        $this->errors = [];
    }

    /**
     * @param UserRegisterData $userData
     * @return bool
     */
    public function validate(UserRegisterData $userData): bool
    {
        $isValid = true;
        if ($this->usernameValidator->isValid($userData) === false )
        {
           $this->addError($this->usernameValidator->getFieldName(), $this->usernameValidator->getErrors());
           $isValid = false;
        }

        return $isValid;
    }

    /**
     * @param string $fieldName
     * @param array $errors
     */
    private function addError(string $fieldName, array $errors)
    {
        if(isset($this->errors[$fieldName]))
        {
            $this->errors[$fieldName][] = $errors;
        }
        else
        {
            $this->errors[$fieldName] = $errors;
        }
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}