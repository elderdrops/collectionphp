<?php
/**
 * Created by PhpStorm.
 * User: ElderDrop
 * Date: 27.10.18
 * Time: 09:00
 */

namespace App\Infrastructure\User\Builder;


use App\Domain\User\Builder\UserBuilderInterface;
use App\Domain\User\Exception\InvalidBuildDataException;
use App\Domain\User\Facade\Register\UserDataRegisterInterface;
use App\Domain\User\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserRegisterBuilder implements UserBuilderInterface
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * UserRegisterBuilder constructor.
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }


    /**
     * @param UserDataRegisterInterface $data
     * @return User
     * @throws InvalidBuildDataException
     */
    public function build($data): User
    {
        if(($data instanceof UserDataRegisterInterface) === false)
        {
            $e = new InvalidBuildDataException();
            $e->setData($data);
            throw $e;
        }

        $user = new User();
        $user->setUsername($data->getUsername());
        $user->setPassword(
            $this->encoder->encodePassword($user, $data->getPassword())
        );

        return $user;
    }
}