<?php
/**
 * Created by PhpStorm.
 * User: ElderDrop
 * Date: 13.11.18
 * Time: 21:38
 */

namespace App\Infrastructure\Comic\Service;


use App\Domain\Comic\Comic;
use App\Domain\Comic\Repository\ComicRepositoryInterface;
use App\Domain\Comic\Service\AssignServiceInterface;
use App\Domain\User\Repository\UserRepositoryInterface;
use App\Domain\User\User;
use Doctrine\ORM\EntityManagerInterface;

class AssignComicService implements AssignServiceInterface
{

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;
    /**
     * @var ComicRepositoryInterface
     */
    private $comicRepository;

    public function __construct(UserRepositoryInterface $userRepository, ComicRepositoryInterface $comicRepository)
    {

        $this->userRepository = $userRepository;
        $this->comicRepository = $comicRepository;
    }

    /**
     * @param Comic $comic
     * @param User $user
     */
    public function assignOwnerToComic(Comic $comic, User $user): void
    {
        $user->addComic($comic);
        //TODO: Exceptiony
        $this->userRepository->saveUser($user);
        $this->comicRepository->saveComic($comic);

    }
}