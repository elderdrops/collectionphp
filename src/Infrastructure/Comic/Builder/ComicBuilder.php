<?php

namespace App\Infrastructure\Comic\Builder;


use App\Domain\Comic\Builder\ComicBuilderInterface;
use App\Domain\Comic\Comic;
use App\Domain\Comic\Validator\ComicNewRequestValidatorInterface;
use App\Domain\Comic\ValueObject\Description;
use App\Domain\Comic\ValueObject\Title;
use Symfony\Component\HttpFoundation\Request;

class ComicBuilder implements ComicBuilderInterface
{
    /**
     * @param Request $request
     * @return Comic
     */
    public function build(Request $request): Comic
    {
        $comic = new Comic();

        $comic->setTitle(Title::fromString($request->request->get('title')));

        $comic->setDescription(
            Description::fromString($request
                ->request
                ->get('description', ''))
        );

        return $comic;
    }
}