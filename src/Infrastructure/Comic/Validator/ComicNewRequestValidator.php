<?php
/**
 * Created by PhpStorm.
 * User: ElderDrop
 * Date: 28.10.18
 * Time: 13:39
 */

namespace App\Infrastructure\Comic\Validator;


use App\Domain\Comic\Exception\NotUniqueTitleException;
use App\Domain\Comic\Repository\ComicRepositoryInterface;
use App\Domain\Comic\Validator\ComicNewRequestValidatorInterface;
use App\Domain\Comic\ValueObject\Title;
use Assert\Assertion;
use Symfony\Component\HttpFoundation\Request;

class ComicNewRequestValidator implements ComicNewRequestValidatorInterface
{
    /**
     * @var ComicRepositoryInterface
     */
    private $repository;

    /**
     * ComicNewRequestValidator constructor.
     * @param ComicRepositoryInterface $repository
     */
    public function __construct(ComicRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @throws \Assert\AssertionFailedException
     * @throws NotUniqueTitleException
     */
    public function isValid(Request $request): void
    {
        $title = $request->request->get('title');
        Assertion::notNull($title);

        if($this->repository->findOneByTitle(Title::fromString($title)))
        {
            throw new NotUniqueTitleException();
        }

    }
}