<?php
/**
 * Created by PhpStorm.
 * User: ElderDrop
 * Date: 28.10.18
 * Time: 11:53
 */

namespace App\Infrastructure\Comic\Repository;


use App\Domain\Comic\Comic;
use App\Domain\Comic\Repository\ComicRepositoryInterface;
use App\Domain\Comic\ValueObject\Title;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ComicRepository extends ServiceEntityRepository implements ComicRepositoryInterface
{

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Comic::class);
    }

    public function findOneByTitle(Title $title): ?Comic
    {
        return $this->createQueryBuilder('u')
            ->where('u.title.title = :title')
            ->setParameter('title', $title->toString())
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param Comic $comic
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveComic(Comic $comic): void
    {
        $this->_em->persist($comic);
        $this->_em->flush();

    }
}